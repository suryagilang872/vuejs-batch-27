/*soal 1
buatlah variabel seperti di bawah ini

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
urutkan array di atas dan tampilkan data seperti output di bawah ini(dengan menggunakan loop):

1. Tokek
2. Komodo
3. Cicak
4. Ular
5. Buaya
*/
//----------------------------------------JAWABAN SOAL NO 1----------------------------------------------------------//
console.log("------------------------------------------")
console.log("Soal 1")

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var cek = daftarHewan.length;
for (var i = 0; i < cek; i++) {
    daftarHewan.sort()
    console.log(daftarHewan[i]);
}

//-------------------------------------------------------------------------------------------------------------------//
/*soal 2
Tulislah sebuah
function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!"
*/
/* 
    Tulis kode function di sini
*/

/*var data = {
    name: "John",
    age: 30,
    address: "Jalan Pelesiran",
    hobby: "Gaming"
}

var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"
*/

//----------------------------------------JAWABAN SOAL NO 2----------------------------------------------------------//
console.log("------------------------------------------")
console.log("Soal 2")
var data = {
    name: "John",
    age: 30,
    address: "Jalan Pelesiran",
    hobby: "Gaming"
}

function introduce(data) {
    return "Nama Saya " + data.name + "," + " umur saya " + data.age + " tahun," + " alamat saya di " +
        data.address + "," + " dan saya punya hobby yaitu " + data.hobby
}
var perkenalan = introduce(data)
console.log(perkenalan)


//-------------------------------------------------------------------------------------------------------------------//
/*soal 3
Tulislah sebuah
function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1, hitung_2) // 3 2
*/

//----------------------------------------JAWABAN SOAL NO 3----------------------------------------------------------//
console.log("------------------------------------------")
console.log("Soal 3")

function hitung_huruf_vokal(params) {
    var vocalCount = 0
    for (const i in params) {
        var kata = params[i].toLowerCase()
        if (kata == "a" || kata == "i" || kata == "u" || kata == "e" || kata == "o") {
            vocalCount += 1
        }
    }
    return vocalCount
}
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1, hitung_2) // 3 2

//-------------------------------------------------------------------------------------------------------------------//
/**
 * Buatlah sebuah
 function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut.

 console.log(hitung(0)) // -2
 console.log(hitung(1)) // 0
 console.log(hitung(2)) // 2
 console.log(hitung(3)) // 4
 console.log(hitung(5)) // 8

 */

//----------------------------------------JAWABAN SOAL NO 4----------------------------------------------------------//
console.log("------------------------------------------")
console.log("Soal 4")

function hitung(params) {
    return params * 2 - 2
}

console.log(hitung(0)) // -2
console.log(hitung(1)) // 0
console.log(hitung(2)) // 2
console.log(hitung(3)) // 4
console.log(hitung(5)) // 8