/*soal 1

buatlah variabel seperti di bawah ini

var nilai;
pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut.lalu buat lah pengkondisian dengan
if -elseif dengan kondisi

nilai >= 85 indeksnya A
nilai >= 75 dan nilai < 85 indeksnya B
nilai >= 65 dan nilai < 75 indeksnya c
nilai >= 55 dan nilai < 65 indeksnya D
nilai < 55 indeksnya E
*/

//----------------------------------------JAWABAN SOAL NO 1----------------------------------------------------------//
console.log("------------------------------------------")
console.log("Soal 1")

var nilai = 5;

if (nilai >= 85) {
    console.log("Nilai Anda Adalah = A")
} else if (nilai >= 75) {
    console.log(nilai, "Nilai Anda Adalah = B")
} else if (nilai >= 65) {
    console.log(nilai, "Nilai Anda Adalah = C")
} else if (nilai >= 55) {
    console.log(nilai, "Nilai Anda Adalah = D")
} else if (nilai < 55) {
    console.log(nilai, "Nilai Anda Adalah = E")
}

//-------------------------------------------------------------------------------------------------------------------//
/*soal 2

buatlah variabel seperti di bawah ini

var tanggal = 22;
var bulan = 7;
var tahun = 2020;
ganti tanggal, bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah
switch
case pada bulan, lalu muncul kan string nya dengan output seperti ini 22 Juli 2020(isi di sesuaikan dengan tanggal lahir masing - masing)
*/

//----------------------------------------JAWABAN SOAL NO 2----------------------------------------------------------//
console.log("------------------------------------------")
console.log("Soal 2")

var tanggal = 2;
var bulan = 6;
var tahun = 2001;

var bulan;
switch (bulan) {
    case 1: {
        console.log('1 Juni 2021');
        break;
    }
    case 2: {
        console.log('2 Juni 2021');
        break;
    }
    case 3: {
        console.log('3 Juni 2021');
        break;
    }
    case 4: {
        console.log('4 Juni 2021');
        break;
    }
    case 5: {
        console.log('5 Juni 2021');
        break;
    }
    case 6: {
        console.log(tanggal, 'Juni', tahun);
        break;
    }
    default: {
        console.log('ERROR');
    }
}

//-------------------------------------------------------------------------------------------------------------------//
/*soal 3
Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar(#) dengan dimensi tinggi n dan alas n.
Looping boleh menggunakan syntax apa pun(while, for, do while).

Output untuk n = 3:

#
##
###

Output untuk n = 7:

#
##
###
####
#####
######
#######
*/

//----------------------------------------JAWABAN SOAL NO 3----------------------------------------------------------//
console.log("------------------------------------------")
console.log("Soal 3")

var n = '';

console.log("Hasil n = 3")
for (var i = 1; i <= 3; i++) {
    for (var j = 1; j <= i; j++) {
        n += '*';
    }
    n += '\n';
}
console.log(n, "\n");

var n2 = '';
console.log("Hasil n = 7")
for (var i = 1; i <= 7; i++) {
    for (var j = 1; j <= i; j++) {
        n2 += '*';
    }
    n2 += '\n';
}
console.log(n2);

//-------------------------------------------------------------------------------------------------------------------//
/*soal 4
berilah suatu nilai m dengan tipe integer, dan buatlah pengulangan dari 1 sampai dengan m, dan berikan output sebagai berikut.
contoh:

Output untuk m = 3

1 - I love programming
2 - I love Javascript
3 - I love VueJS 
===

Output untuk m = 5

1 - I love programming
2 - I love Javascript
3 - I love VueJS 
===
4 - I love programming
5 - I love Javascript

Output untuk m = 7

1 - I love programming
2 - I love Javascript
3 - I love VueJS 
===
4 - I love programming
5 - I love Javascript
6 - I love VueJS 
======
7 - I love programming


Output untuk m = 10

1 - I love programming
2 - I love Javascript
3 - I love VueJS 
===
4 - I love programming
5 - I love Javascript
6 - I love VueJS 
======
7 - I love programming
8 - I love Javascript
9 - I love VueJS 
=========
10 - I love programming
*/

//----------------------------------------JAWABAN SOAL NO 3----------------------------------------------------------//
console.log("------------------------------------------")
console.log("Soal 4")


console.log("Hasil m = 3")
for (var m = 1; m < 4; m += 1) {
    console.log(m, "- I love programming");
}