var readBooksPromise = require('./promise.js')

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
  { name: 'komik', timeSpent: 1000 },
]

// Lanjutkan code untuk menjalankan function readBooksPromise
function mulaiBaca(waktuSisa, books, index) {
  let baca = readBooksPromise(waktuSisa, books[index])
  baca.then((content) => {
    const nextBook = index + 1
    if (nextBook < books.length) {
      mulaiBaca(content, books, nextBook)
    }
  })
}
mulaiBaca(10000, books, 0)
