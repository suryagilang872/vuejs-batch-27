var sayHello = "Hello world"
console.log(sayHello, "\n")

//-------------------------------------------------------------------------------------------------------------------//

/*Soal 1
buatlah variabel - variabel seperti di bawah ini
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

console.log(pertamaq.concat(keduaq));

gabungkan variabel - variabel tersebut agar menghasilkan output

Hasil :
saya senang belajar JAVASCRIPT
*/

//----------------------------------------JAWABAN SOAL NO 1----------------------------------------------------------//
console.log("------------------------------------------")
console.log("Soal 1")

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var pertamaq = pertama.substr(0, 4);
var pertamaqq = pertama.substr(11, 8);
var keduaq = kedua.substr(0, 18);
console.log(pertamaq.concat(pertamaqq).concat(keduaq), "\n");

//-------------------------------------------------------------------------------------------------------------------//

/*soal 2

buatlah variabel - variabel seperti di bawah ini

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
ubahlah variabel diatas ke dalam integer dan lakukan operasi matematika semua variabel agar menghasilkan output 24(integer).*catatan:
    1. gunakan 3 operasi, tidak boleh lebih atau kurang.contoh: 10 + 2 * 4 + 6
2. Boleh menggunakan tanda kurung.contoh: (10 + 2) * (4 + 6)
*/

//----------------------------------------JAWABAN SOAL NO 2----------------------------------------------------------//
console.log("------------------------------------------")
console.log("Soal 2")

var kataPertama = "10";
var strInt_1 = parseInt(kataPertama); // perubahan menjadi integer strInt_1 = 10
var kataKedua = "2";
var strInt_2 = parseInt(kataKedua); // perubahan menjadi integer strInt_2 = 2
var kataKetiga = "4";
var strInt_3 = parseInt(kataKetiga); // perubahan menjadi integer strInt_3 = 4
var kataKeempat = "6";
var strInt_4 = parseInt(kataKeempat); // perubahan menjadi integer strInt_4 = 6

var Hasil_1 = (strInt_1 * strInt_2) + strInt_3;
console.log('Hasil Penjumlahan 10 * 2 + 4 = ' + Hasil_1, "\n")

var Hasil_2 = strInt_1 + strInt_1 + strInt_3
console.log('Hasil Penjumlahan 10 * 10 + 2 = ' + Hasil_2, "\n")

//-------------------------------------------------------------------------------------------------------------------//

/*soal 3

buatlah variabel - variabel seperti di bawah ini

var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua; // do your own! 
var kataKetiga; // do your own! 
var kataKeempat; // do your own! 
var kataKelima; // do your own! 

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);
selesaikan variabel yang belum diisi dan hasilkan output seperti berikut:

Kata Pertama: wah
Kata Kedua: javascript
Kata Ketiga: itu
Kata Keempat: keren
Kata Kelima: sekali
*/

//----------------------------------------JAWABAN SOAL NO 3----------------------------------------------------------//

console.log("------------------------------------------")
console.log("Soal 3")

var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);